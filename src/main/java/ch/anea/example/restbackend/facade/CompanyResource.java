package ch.anea.example.restbackend.facade;

import ch.anea.example.restbackend.facade.dto.ContractDto;
import ch.anea.example.restbackend.persistence.dataaccess.CompanyDao;
import ch.anea.example.restbackend.persistence.dataaccess.ContractDao;
import ch.anea.example.restbackend.persistence.entity.Company;
import ch.anea.example.restbackend.persistence.entity.Contract;
import ch.anea.example.restbackend.service.CompanyService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Stateless
@Path("company")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CompanyResource {
    @EJB
    private CompanyDao companyDao;
    @EJB
    private ContractDao contractDao;
    @EJB
    private CompanyService companyService;

    @GET
    @Path("{companyName}")
    public Company getCompany(@PathParam("companyName") String companyName) {
        Company company = companyDao.getByName(companyName);
        if (company == null) {
            throw new NotFoundException();
        }

        return company;
    }

    @PUT
    public void createCompany(Company company) {
        companyDao.createCompany(company);
    }

    @GET
    @Path("{companyName}/contracts")
    public List<Contract> getContracts(@PathParam("companyName") String companyName) {
        List<Contract> contracts = contractDao.findContractsToCompany(companyName);
        return contracts;
    }


    @PUT
    @Path("{companyName}/contract")
    public void createContract(@PathParam("companyName") String companyName, Contract contract) {
        Company company = companyDao.getByName(companyName);
        if (company == null) {
            throw new NotFoundException();
        }
        contract.setCompany(company);
        contractDao.createContract(contract);
    }

    @GET
    @Path("findAll")
    public List<Company> findAll() {
        List<Company> companyList = companyDao.findAll();
        return companyList;
    }

    @GET
    @Path("findAllContracts")
    public List<ContractDto> findAllContracts() {
        List<ContractDto> contractDtoList = new ArrayList<>();

        List<Contract> contracts = contractDao.findAll();
        for (Contract contract : contracts) {
            ContractDto dto = new ContractDto();
            contractDtoList.add(dto);
            dto.setCompanyName(contract.getCompany().getCompanyName());
            dto.setName(contract.getName());
        }

        return contractDtoList;
    }

    @POST
    @Path("updateFromZefix/{companyName}")
    public void updateCompanyFromZefix(@PathParam("companyName") String companyName) {
        companyService.updateCompanyStatusFromZefix(companyName);
    }


}
