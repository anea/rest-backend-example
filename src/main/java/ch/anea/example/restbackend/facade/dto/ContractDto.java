package ch.anea.example.restbackend.facade.dto;

public class ContractDto {
    private String companyName;
    private String name;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
