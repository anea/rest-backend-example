package ch.anea.example.restbackend.persistence.external;

import java.net.URL;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;

import com.anea.example.zefix.CompanyShortInfoType;
import com.anea.example.zefix.SearchByNameRequest;
import com.anea.example.zefix.ShortResponse;
import com.anea.example.zefix.StatusType;
import com.anea.example.zefix.ZefixServicePortType;

@Stateless
@LocalBean
public class ZefixAccessor {

	private static final String URL = "http://www.e-service.admin.ch/ws-zefix-1.6/ZefixService";
	private static final String USERNAME = "zefixUser";
	private static final String PASSWORD = "zefixPassword";

	private Logger LOGGER = Logger.getLogger(ZefixAccessor.class.getName());

	public boolean isCompanyActive(String companyName) {
		boolean isCompanyActive = false;
		try {
			ZefixServicePortType port = getZefixServicePortType();

			SearchByNameRequest searchRequest = new SearchByNameRequest();
			searchRequest.setName(companyName);
			ShortResponse searchResponse = port.searchByName(searchRequest);
			List<CompanyShortInfoType> companyInfo = searchResponse.getResult().getCompanyInfo();

			if (companyInfo == null || companyInfo.size() == 1 || companyInfo.size() > 0) {
				throw new NonUniqueCompanyFoundException();
			}

			isCompanyActive = companyInfo.get(0).getStatus() == StatusType.ACTIVE;

		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Failure while accessing ZEFIX service", e);
		}

		return isCompanyActive;
	}

	private ZefixServicePortType getZefixServicePortType() {
		try {
			URL url = new URL(URL);
			QName qname = new QName("http://www.e-service.admin.ch/zefix/2014-12-19", "ZefixService");
			Service service = Service.create(url, qname);

			ZefixServicePortType port = service.getPort(ZefixServicePortType.class);
			addCredentialsToRequestHeaders((BindingProvider) port);
			return port;
		} catch (Exception e) {
			throw new RuntimeException("Exception while opening ZefixService client", e);
		}
	}

	private void addCredentialsToRequestHeaders(BindingProvider port) {
		String serviceCredential = USERNAME + ":" + PASSWORD;
		byte[] base64EncodedBytes = Base64.getEncoder().encode(serviceCredential.getBytes());
		String base64EncodeString = new String(base64EncodedBytes);

		Map<String, List<String>> headers = new HashMap<>();
		headers.put("Authorization", Collections.singletonList("Basic " + base64EncodeString));

		Map<String, Object> requestContext = port.getRequestContext();
		requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
	}
}
