package ch.anea.example.restbackend.persistence.dataaccess;

import ch.anea.example.restbackend.persistence.entity.Contract;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
@LocalBean
public class ContractDao {
    @PersistenceContext
    private EntityManager em;


    public void createContract(Contract contract) {
        em.persist(contract);
    }

    public List<Contract> findAll() {
        String ql = "select c from Contract c";
        TypedQuery<Contract> query = em.createQuery(ql, Contract.class);
        return query.getResultList();
    }

    public List<Contract> findContractsToCompany(String companyName) {
        String ql = "select c from Contract c where c.company.companyName = :companyName";
        TypedQuery<Contract> query = em.createQuery(ql, Contract.class);
        query.setParameter("companyName", companyName);

        return query.getResultList();
    }
}
