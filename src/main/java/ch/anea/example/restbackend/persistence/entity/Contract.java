package ch.anea.example.restbackend.persistence.entity;

import javax.persistence.*;

@Entity
public class Contract {
    @Id
    @GeneratedValue
    private long id;
    @Version
    private int version;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Company company;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
