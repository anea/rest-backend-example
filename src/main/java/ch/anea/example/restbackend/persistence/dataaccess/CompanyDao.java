package ch.anea.example.restbackend.persistence.dataaccess;

import ch.anea.example.restbackend.persistence.entity.Company;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
@LocalBean
public class CompanyDao {
    @PersistenceContext
    private EntityManager em;

    public Company getByName(String companyName) {
        Company retVal = null;
        String ql = "select c from Company c where c.companyName = :companyName";
        TypedQuery<Company> query = em.createQuery(ql, Company.class);
        query.setParameter("companyName", companyName);
        List<Company> resultList = query.getResultList();
        if (resultList.size() > 0) {
            retVal = resultList.get(0);
        }
        return retVal;
    }

    public void createCompany(Company company) {
        em.persist(company);
    }

    public List<Company> findAll() {
        String ql = "select c from Company c";
        TypedQuery<Company> query = em.createQuery(ql, Company.class);
        return query.getResultList();
    }
}
