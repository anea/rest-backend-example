package ch.anea.example.restbackend.persistence.entity;

import javax.persistence.*;

@Entity
public class Company {
    @Id
    @GeneratedValue
    private long id;
    @Version
    private int version;
    @Column(name = "company_name", unique = true, nullable = false)
    private String companyName;
    @Column(name = "activeInZefix")
    private boolean activeInZefix;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Boolean getActiveInZefix() {
        return activeInZefix;
    }

    public void setActiveInZefix(Boolean activeInZefix) {
        this.activeInZefix = activeInZefix;
    }
}
