package ch.anea.example.restbackend.service;

import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.NotFoundException;

import ch.anea.example.restbackend.persistence.dataaccess.CompanyDao;
import ch.anea.example.restbackend.persistence.entity.Company;
import ch.anea.example.restbackend.persistence.external.ZefixAccessor;

@Stateless
@LocalBean
public class CompanyService {
    private Logger LOGGER = Logger.getLogger(CompanyService.class.getName());

    @EJB
    private CompanyDao companyDao;
    @EJB
    private ZefixAccessor zefixAccessor;

    public void updateCompanyStatusFromZefix(String companyName) {
        Company company = companyDao.getByName(companyName);
        if (company == null) {
            throw new NotFoundException();
        }

        boolean isCompanyActive = zefixAccessor.isCompanyActive(companyName);
        LOGGER.info("Setting company " + companyName + " to ZEFIX status active: " + isCompanyActive);

        company.setActiveInZefix(isCompanyActive);
    }
}
