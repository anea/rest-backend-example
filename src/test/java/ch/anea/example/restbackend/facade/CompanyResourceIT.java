package ch.anea.example.restbackend.facade;

import ch.anea.example.restbackend.facade.dto.ContractDto;
import ch.anea.example.restbackend.persistence.entity.Company;
import ch.anea.example.restbackend.persistence.entity.Contract;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class CompanyResourceIT {
    private static final String URI = "http://localhost:8080/rest-backend-example/resources/company/";
    private Client client;

    @BeforeEach
    public void setup() {
        client = ClientBuilder.newClient();
        //uncomment for more loggin
        //Logger logger = Logger.getLogger(getClass().getName());
        //client.register(new LoggingFilter(logger, true));
    }

    @Test
    public void testGetCompanyNotFound() {
        assertThrows(NotFoundException.class, () -> {
            client.target(URI)
                    .path("notthere")
                    .request()
                    .get(String.class);
        });
    }

    @Test
    public void testCreateAndGetCompany() {
        Company companyIn = new Company();
        companyIn.setCompanyName(UUID.randomUUID().toString());
        Response response = client.target(URI)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .put(Entity.entity(companyIn, MediaType.APPLICATION_JSON_TYPE));
        assertEquals(204, response.getStatus());

        //now we try to read this company
        Company company = client.target(URI)
                .path(companyIn.getCompanyName())
                .request()
                .get(Company.class);
        assertNotNull(company);
        assertEquals(companyIn.getCompanyName(), company.getCompanyName());
    }

    @Test
    public void testFindAll() {
        //create some companies
        for (int i = 0; i < 10; i++) {
            testCreateAndGetCompany();
        }

        List<Company> companyList = client.target(URI)
                .path("findAll")
                .request()
                .get()
                .readEntity(new GenericType<List<Company>>() {
                });
        assertTrue(companyList.size() > 9);
    }

    @Test
    public void testFindAllContracts() {
        //create some companies
        for (int i = 0; i < 10; i++) {
            testCreateAndGetContract();
        }

        List<ContractDto> contractDtoList = client.target(URI)
                .path("findAllContracts")
                .request()
                .get()
                .readEntity(new GenericType<List<ContractDto>>() {
                });
        assertTrue(contractDtoList.size() > 9);
    }


    @Test
    public void testUpdateStatus() {
        String companyName = "anea GmbH";

        // Check if anea GmbH already exists in the database
        try {
            client.target(URI).path(companyName).request().get(Company.class);
        } catch (javax.ws.rs.NotFoundException e) {

            // Company 'anea' not found in database --> create a new entry
            Company companyIn = new Company();
            companyIn.setCompanyName(companyName);
            client.target(URI).request(MediaType.APPLICATION_JSON_TYPE).put(Entity.entity(companyIn, MediaType.APPLICATION_JSON_TYPE));
        }

        // Now run the 'updateCompanyStatusFromZefix'
        client.target(URI).path("updateFromZefix").path(companyName).request().post(null);

        // Finally validate the ZEFIX status flag
        Company company = client.target(URI).path(companyName).request().get(Company.class);
        assertEquals(company.getActiveInZefix(), false);
    }

    @Test
    public void testCreateAndGetContract() {
        Company companyIn = new Company();
        companyIn.setCompanyName(UUID.randomUUID().toString());
        client.target(URI)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .put(Entity.entity(companyIn, MediaType.APPLICATION_JSON_TYPE));

        Contract contractIn = new Contract();
        contractIn.setName(UUID.randomUUID().toString());
        Response response = client.target(URI)
                .path(companyIn.getCompanyName())
                .path("contract")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .put(Entity.entity(contractIn, MediaType.APPLICATION_JSON_TYPE));
        assertEquals(204, response.getStatus());

        //now we try to read this company
        List<Contract> contracts = client.target(URI)
                .path(companyIn.getCompanyName())
                .path("contracts")
                .request()
                .get().readEntity(new GenericType<List<Contract>>() {
                });
        assertEquals(1, contracts.size());
        assertEquals(contractIn.getName(), contracts.get(0).getName());
    }
}
